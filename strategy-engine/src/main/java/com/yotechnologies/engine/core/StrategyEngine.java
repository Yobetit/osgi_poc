package com.yotechnologies.engine.core;

import com.yotechnologies.engine.model.StrategySolution;
import com.yotechnologies.engine.model.StrategySolutionManager;
import org.apache.log4j.Logger;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.List;

/**
 * Created by jab on 15/01/2015.
 */
public class StrategyEngine {

    private static final Logger logger = Logger.getLogger(StrategyEngine.class);

    private StrategySolutionManager strategySolutionManager;

    @Scheduled(fixedDelay = 20000)
    public void executeStrategies() throws Exception {

        logger.info("Getting available strategies...");
        List<String> availableStrategies = strategySolutionManager.getAvailableStrategySolutions();
        logger.info("Retrieved strategies : "+ + availableStrategies.size());

        for (String strategyName : availableStrategies) {

            logger.info("Getting reference to  : " + strategyName);
            StrategySolution strategy = strategySolutionManager.getStrategySolution(strategyName);

            if (strategy != null) {
                strategy.calculateStrategy();
            } else {
                logger.warn("Reference is null!");
            }
        }

        logger.info("Finished executing available strategies.");


    }

    public StrategySolutionManager getStrategySolutionManager() {
        return strategySolutionManager;
    }

    public void setStrategySolutionManager(StrategySolutionManager strategySolutionManager) {
        this.strategySolutionManager = strategySolutionManager;
    }
}
