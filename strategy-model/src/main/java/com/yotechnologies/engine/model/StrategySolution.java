package com.yotechnologies.engine.model;

/**
 * Strategy Solution
 * 
 */
public interface StrategySolution {

    /**
     * Gets unique strategy solution name
     * 
     * @return strategy solution name
     */
    String getName();

    /**
     * Executes a strategy stage
     * 
     * @throws Exception
     *             when error occurs
     */
    void calculateStrategy() throws Exception;


}
