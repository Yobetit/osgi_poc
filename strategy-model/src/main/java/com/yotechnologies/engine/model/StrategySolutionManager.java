package com.yotechnologies.engine.model;

import org.apache.log4j.Logger;
import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.ArrayList;
import java.util.List;

/**
 * Strategy Solution Manager
 *
 * This class keeps track of all solutions bundles , and is used by the engine to call instructions on them.
 * 
 */
public class StrategySolutionManager implements InitializingBean, DisposableBean {

    private static final Logger logger = Logger.getLogger(StrategySolutionManager.class);
    
    @Autowired
    private BundleContext bundleContext;

    private ServiceTracker<StrategySolution, Object> serviceTracker;

    @Scheduled(fixedDelay = 60000)
    public void monitorStrategySolutuoins() throws Exception {

        logger.info("Strategy monitor : Getting all available strategies...");
        List<String> solutions = getAvailableStrategySolutions();
        logger.info("Strategy monitor : Retrieved strategy count : " + solutions.size());

    }

    /**
     * Returns a name list of all registered solution bundles.
     *
     * @return List<String>
     */
    public List<String> getAvailableStrategySolutions() {

        List<String> strategyList = new ArrayList<String>();

        if (!serviceTracker.isEmpty()) {
            Object[] services = serviceTracker.getServices();

            for (Object o : services) {
                StrategySolution ss = (StrategySolution) o;
                String strategyName = ss.getName();

                strategyList.add(strategyName);
                logger.info("Added to list : " + strategyName);

            }
        } else {
            logger.debug("Strategy solution tracker is empty.");
        }

        return strategyList;
    }

    /**
     * Gets strategy solution by name
     * 
     * @param name
     * @return payment solution or {@code null} when not found
     */
    public StrategySolution getStrategySolution(final String name) {
        StrategySolution result = null;

        if (!serviceTracker.isEmpty()) {
            Object[] services = serviceTracker.getServices();

            for (Object o : services) {
                final StrategySolution ss = (StrategySolution) o;
                final String inputName = name;
                final String strategyName = ss.getName();
                
                if (inputName.equalsIgnoreCase(strategyName)) {
                    result = ss;
                    break;
                }
            }
        } else {
            logger.debug("Strategy solution tracker is empty.");
        }

        return result;
    }

    @Override
    public void afterPropertiesSet() throws Exception {

        logger.info("Opening service tracker....");

        serviceTracker = new ServiceTracker<StrategySolution, Object>(
                bundleContext, StrategySolution.class, null);
        serviceTracker.open();

        logger.info("Service tracker opened.");
    }

    @Override
    public void destroy() throws Exception {
        serviceTracker.close();
    }


}
