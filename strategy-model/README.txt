Required 3rd party bundles.

*******
Commons
*******

install -s mvn:org.apache.commons/commons-lang3

**************
Spring bundles
**************

from : http://search.maven.org/#search%7Cga%7C1%7Cservicemix%20spring

install  -s mvn:org.apache.servicemix.bundles/org.apache.servicemix.bundles.spring-core/4.1.2.RELEASE_1
install  -s mvn:org.apache.servicemix.bundles/org.apache.servicemix.bundles.spring-beans/4.1.2.RELEASE_1
install  -s mvn:org.apache.servicemix.bundles/org.apache.servicemix.bundles.spring-context/4.1.2.RELEASE_1


