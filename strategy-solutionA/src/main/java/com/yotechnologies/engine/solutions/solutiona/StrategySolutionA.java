package com.yotechnologies.engine.solutions.solutiona;

import com.yotechnologies.engine.model.StrategySolution;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * A strategy solution
 *
 */
public class StrategySolutionA implements StrategySolution {
	
	private static final Logger logger = Logger.getLogger(StrategySolutionA.class);
	
	@Autowired
	private DummyLogicA dummyLogic;


	@Override
	public String getName() {
		return "StrategySolutionA";
	}

	@Override
	public void calculateStrategy() throws Exception {

		dummyLogic.doSomething();
	}
}
