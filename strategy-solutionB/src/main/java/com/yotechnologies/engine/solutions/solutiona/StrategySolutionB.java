package com.yotechnologies.engine.solutions.solutiona;

import com.yotechnologies.engine.model.StrategySolution;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * A strategy solution
 *
 */
public class StrategySolutionB implements StrategySolution {
	
	private static final Logger logger = Logger.getLogger(StrategySolutionB.class);
	
	@Autowired
	private DummyLogicB dummyLogic;


	@Override
	public String getName() {
		return "StrategySolutionB";
	}

	@Override
	public void calculateStrategy() throws Exception {

		dummyLogic.doSomething();
	}
}
