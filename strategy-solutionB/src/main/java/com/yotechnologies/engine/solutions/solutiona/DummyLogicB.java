package com.yotechnologies.engine.solutions.solutiona;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;


/**
 *
 * Class exposing some logic....  The usual stuff.
 *
 */
@Component
public class DummyLogicB {
	
	private static final Logger logger = Logger.getLogger(DummyLogicB.class);
	

	public void doSomething() {

		// your usual logic here.... etc etc.
		logger.info("This is solution B.");

	}
}
